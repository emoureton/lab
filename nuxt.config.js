export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Lab - Etienne Moureton.fr",
    htmlAttrs: {
      lang: "fr",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    // script: [
    //   { src: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.10.3/gsap.min.js" },
    // ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    {
      src: "~/assets/styles/global.scss",
      lang: "scss",
    },
  ],

  styleResources: {
    scss: [
      "~/assets/styles/_breakpoints.scss",
      "~/assets/styles/_animations.scss",
      "~/assets/styles/_variables.scss",
    ],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: "~/plugins/three.js", ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [{ path: "~/components", extensions: ["vue"] }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    [
      "@nuxtjs/google-fonts",
      {
        families: {
          Poppins: {
            wght: [300, 400, 600, 700],
          },
        },
        display: "swap",
        prefetch: true,
        preconnect: true,
        preload: true,
        download: true,
        base64: false,
        stylePath: "styles/_fonts.scss",
      },
    ],
    "nuxt-gsap-module",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxtjs/style-resources", "@nuxtjs/svg"],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: "/",
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      });
    },
  },

  gsap: {
    clubPlugins: {
      splitText: true,
    },
    extraPlugins: {
      scrollTrigger: true,
    },
  },
};
