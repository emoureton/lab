import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Group,
  Mesh,
  PCFSoftShadowMap,
  ACESFilmicToneMapping,
  Color,
  sRGBEncoding,
  AmbientLight,
  SpotLight,
  MeshLambertMaterial,
  BoxGeometry,
  Raycaster,
  Vector2,
} from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";

export default class CreationsScene {
  constructor({ context }) {
    this.context = context;
    this.gsap = context.$gsap;
    this.settings = {
      frontLight: {
        color: new Color(0xbe7c7c),
        intensity: 10.6,
        distance: 26.9,
        penumbra: 0,
        x: 10,
        y: 14,
        z: 20,
      },
      backLight: {
        color: new Color(0xc9f0f0),
        intensity: 10,
        distance: 23,
        penumbra: 0,
        x: -19,
        y: -5.3,
        z: 3,
      },
    };
  }

  init({ container }) {
    this.container = container;
    this.createScene();
  }

  createScene() {
    const pixelRatio = window.devicePixelRatio;

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
    });

    this.renderer.setPixelRatio(pixelRatio);

    this.renderer.toneMappingExposure = 0.6;
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.toneMapping = ACESFilmicToneMapping;
    this.renderer.powerPreference = "high-performance";

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = PCFSoftShadowMap;

    this.container.appendChild(this.renderer.domElement);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(
      50,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );

    this.camera.position.y = 3;
    this.camera.position.z = 10;

    const ambientLight = new AmbientLight("blue");
    this.scene.add(ambientLight);

    this.createLights();
    this.addObjects();
    this.raycast();
    this.onResize();
    this.render();
  }

  raycast() {
    this.raycaster = new Raycaster();
    this.pointer = new Vector2();

    window.addEventListener("pointermove", (e) => this.onPointerMove(e));
  }

  onPointerMove(event) {
    this.pointer.x = (event.offsetX / window.innerWidth) * 2 - 1;
    this.pointer.y = -(event.offsetY / window.innerHeight) * 2 + 1;
  }

  addObjects() {
    this.meshesContainer = new Group();
    this.scene.add(this.meshesContainer);

    // Generating grid
    const gridSize = 30;
    const gap = 0.2;
    const grid = [];

    // Lines
    const gridLine = [];
    for (let i = -(gridSize / 2); i <= gridSize / 2; i++) {
      gridLine.push(i);
    }

    // Columns
    for (let i = -(gridSize / 2); i <= gridSize / 2; i++) {
      grid.push(gridLine);
    }

    //Objects

    const geometry = new BoxGeometry(1, 10, 1);

    grid.forEach((el, index) => {
      el.forEach((x, i) => {
         const material = new MeshLambertMaterial({
           color: 0xc9f0f0,
         });

         const material2 = new MeshLambertMaterial({
           color: 0xbe7c7c,
         });
        const z = index - gridSize / 2;
        let item;
        if (i % 2) {
          item = new Mesh(geometry, material);
        } else {
          item = new Mesh(geometry, material2);
        }
        item.position.set(
          x + x * gap,
         -5,
          z + z * gap
        );
        this.meshesContainer.add(item);
      });
    });
  }

  createLights() {
    this.frontLight = new SpotLight(
      this.settings.frontLight.color,
      this.settings.frontLight.intensity
      // this.settings.frontLight.distance
    );

    this.frontLight.castShadow = true;
    this.frontLight.shadow.mapSize.width = 1024; // default
    this.frontLight.shadow.mapSize.height = 1024; // default
    this.frontLight.shadow.camera.near = 0.5; // default
    this.frontLight.shadow.camera.far = 500;
    this.frontLight.shadow.normalBias = 0.2;

    this.scene.add(this.frontLight);

    this.backLight = new SpotLight(
      this.settings.frontLight.color,
      this.settings.frontLight.intensity
      // this.settings.frontLight.distance
    );
    this.backLight.castShadow = true;
    this.backLight.shadow.mapSize.width = 1024; // default
    this.backLight.shadow.mapSize.height = 1024; // default
    this.backLight.shadow.camera.near = 0.5; // default
    this.backLight.shadow.camera.far = 500;
    this.backLight.shadow.normalBias = 0.2;
    this.backLight.penumbra = this.settings.backLight.penumbra;

    this.scene.add(this.backLight);

    this.frontLight.position.set(
      this.settings.frontLight.x,
      this.settings.frontLight.y,
      this.settings.frontLight.z
    );
    this.backLight.position.set(
      this.settings.backLight.x,
      this.settings.backLight.y,
      this.settings.backLight.z
    );
  }

  render() {

    this.raycaster.setFromCamera( this.pointer, this.camera );

    // calculate objects intersecting the picking ray
    const intersects = this.raycaster.intersectObjects( this.meshesContainer.children );
    if(intersects[0]) {
       const obj = intersects[0].object;
       this.gsap.to(obj.position, {
         duration: 0.5,
         y: -6,
         ease: "power4.inOut",
       });
    }

    this.renderer.render(this.scene, this.camera);

    window.requestAnimationFrame(() => {
      this.render();
    });
  }

  onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  getObject({ name }) {
    console.log(this.scene);
    return this.scene.getObjectByName(name);
  }
}
