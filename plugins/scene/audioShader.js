import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Vector3,
  PCFSoftShadowMap,
  ACESFilmicToneMapping,
  sRGBEncoding,
  Mesh,
  HemisphereLight,
  MeshNormalMaterial,
  PlaneGeometry,
  ShaderMaterial,
  DoubleSide,
  SpotLight,
  SpotLightHelper,
} from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { vertexShader, fragmentShader } from "./shaders/audio";

export default class AudioShader {
  constructor({ context }) {
    this.gsap = context.$gsap;
    this.context = context;
    this.amplitude = 1.5
  }

  init({ container, audio }) {
    this.container = container;
    this.audio = audio;
    this.createScene();
  }

  play() {
    // if (this.audioContext.state !== "running") {
    //   this.audioContext.resume();
    // } else {
    //   this.audioContext.stop();
    // }
    this.audioContext.resume();
  }

  setAmplitude(value) {
    this.amplitude = value
  }

  createScene() {
    const pixelRatio = window.devicePixelRatio;

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
    });
    this.renderer.setPixelRatio(pixelRatio);
    this.renderer.toneMappingExposure = 0.6;
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.toneMapping = ACESFilmicToneMapping;
    this.renderer.powerPreference = "high-performance";
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = PCFSoftShadowMap;

    this.container.appendChild(this.renderer.domElement);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(
      32,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    // this.controls = new OrbitControls(this.camera, this.renderer.domElement);

    this.camera.position.y = 10;
    this.camera.position.z = 50;
    this.camera.lookAt(new Vector3(0, 0, 0));

    this.initAudio()
    this.createLights();
    this.addObjects();
    this.onResize();
    this.render();
  }

  initAudio() {
    this.audioContext = new AudioContext();
    console.log(this.audio)
    this.source = this.audioContext.createMediaElementSource(this.audio);
    this.analyser = this.audioContext.createAnalyser();
    this.source.connect(this.analyser);
    this.analyser.connect(this.audioContext.destination);
    this.analyser.fftSize = 1024;
    this.dataArray = new Uint8Array(this.analyser.frequencyBinCount);
  }

  addObjects() {
    const uniforms = {
      u_time: {
        type: "f",
        value: 1.0,
      },
      u_amplitude: {
        type: "f",
        value: this.amplitude,
      },
      u_data_arr: {
        type: "float[64]",
        value: this.dataArray,
      },
    };

    // Create plane
    const obj = new PlaneGeometry(100, 64, 64, 64);
    const planeCustomShader = new ShaderMaterial({
      uniforms: uniforms,
      vertexShader: vertexShader(),
      fragmentShader: fragmentShader(),
      wireframe: true,
      side: DoubleSide
    })
    const plane = new Mesh(obj, planeCustomShader);
    plane.rotation.x = Math.PI / 2
    plane.castShadow = true;
    plane.receiveShadow = true;
    plane.name = "mainObj";
    this.scene.add(plane);
  }

  createLights() {
    // Create hemisphere light
    const hemisphereLight = new HemisphereLight(0xffffff, 0x080820, 1);
    this.scene.add(hemisphereLight);

    const spotLight = new SpotLight(0xffffff, 1, 0, Math.PI / 4);
    spotLight.target.position.set(new Vector3(0, 0, 0));
    spotLight.castShadow = true;
    spotLight.position.set(20, 20, 0);
    spotLight.name = "spot";
    this.scene.add(spotLight);

    // this.scene.add(new SpotLightHelper(spotLight))
  }

  render() {
    this.renderer.render(this.scene, this.camera);
    // this.controls.update()
    this.analyser.getByteFrequencyData(this.dataArray)

    window.requestAnimationFrame(() => {
      this.render();
    });
  }

  onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  getObject({ name }) {
    return this.scene.getObjectByName(name);
  }
}
