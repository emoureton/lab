import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Vector3,
  PCFSoftShadowMap,
  ACESFilmicToneMapping,
  sRGBEncoding,
  Mesh,
  HemisphereLight,
  PlaneGeometry,
  DoubleSide,
  SpotLight,
  MeshLambertMaterial,
  Group,
} from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
// import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";

export default class TreeScene {
  constructor({ context }) {
    this.gsap = context.$gsap;
    this.context = context;
  }

  init({ container }) {
    this.container = container;
    this.createScene();
  }

  createScene() {
    const pixelRatio = window.devicePixelRatio;

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
    });

    this.renderer.setPixelRatio(pixelRatio);

    this.renderer.toneMappingExposure = 0.6;
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.toneMapping = ACESFilmicToneMapping;
    this.renderer.powerPreference = "high-performance";

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = PCFSoftShadowMap;

    this.container.appendChild(this.renderer.domElement);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(
      32,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    // this.controls = new OrbitControls(this.camera, this.renderer.domElement);

    this.camera.position.y = 40;
    this.camera.position.z = 140;

    this.camera.lookAt(new Vector3(0, 8, 0));

    this.createLights();
    this.addObjects();
    this.onResize();
    this.render();
  }

  addObjects() {
    // Create floor
    const obj = new PlaneGeometry(300, 200, 20, 20);
    const objMat = new MeshLambertMaterial({
      color: 0xE76A46,
      side: DoubleSide,
    });
    const plane = new Mesh(obj, objMat);
    plane.rotation.x = Math.PI / 2;
    plane.castShadow = true;
    plane.receiveShadow = true;
    plane.name = "mainObj";
    this.scene.add(plane);

    this.generateTrees(200)
  }

  handleTrees(instances) {
    // Delete existing tree
    const existingGroup = this.getObject({ name: "TreeGroup" })
    if(existingGroup) {
      if(existingGroup.children.length < instances) {
        this.generateTrees(instances - existingGroup.children.length);
      } else {
        const int = existingGroup.children.length - instances;
        for(let i = 1; i < int; i++) {
          const indexToRemove = Math.round(
            Math.floor(Math.random() * existingGroup.children.length)
          );
          // this.scene.remove(item);
          existingGroup.children.splice(indexToRemove, 1)
        }
      } 
    }
  }

  generateTrees(instances) {
    // Generate trees
    const scene = this.scene;
    const gsap = this.gsap;
    const loader = new GLTFLoader();
    const randomNumber = (min, max) => Math.random() * (max - min) + min;
    const treeGroup = new Group();
    treeGroup.name = "TreeGroup";
    loader.load("/3dmodels/tree_blender_file.glb", function (gltf) {
      const tree = gltf.scene;
      for (let i = 1; i <= instances; i++) {
        const treeInstance = tree.clone();
        treeInstance.position.x = randomNumber(-40, 40);
        treeInstance.position.z = randomNumber(-40, 40);
        treeInstance.scale.set({
          x: 0,
          y: 0,
          z: 0,
        });

        const tl = new gsap.timeline();
        const coeffScale = randomNumber(0.2, 1);
        tl.to(
          treeInstance.scale,
          {
            x: coeffScale,
            y: coeffScale,
            z: coeffScale,
            duration: 0.5,
            ease: "power1.inOut",
          },
          i * 0.01
        );

        treeInstance.castShadow = true;
        treeGroup.add(treeInstance);
      }
    });
    scene.add(treeGroup);
  }

  createLights() {
    // Create hemisphere light
    const hemisphereLight = new HemisphereLight(0xffffff, 0x080820, 1);
    this.scene.add(hemisphereLight);

    const spotLight = new SpotLight(0xffff00, 2, 0, Math.PI / 2);
    spotLight.target.position.set(new Vector3(0, 0, 0));
    spotLight.castShadow = true;
    spotLight.position.set(40, 40, 0);
    spotLight.name = "spot";
    this.scene.add(spotLight);

    // this.scene.add(new SpotLightHelper(spotLight))
  }

  render() {
    this.renderer.render(this.scene, this.camera);
    // this.controls.update()
    window.requestAnimationFrame(() => {
      this.render();
    });
  }

  onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  getObject({ name }) {
    return this.scene.getObjectByName(name);
  }
}
