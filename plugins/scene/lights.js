import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Vector3,
  PCFSoftShadowMap,
  ACESFilmicToneMapping,
  sRGBEncoding,
  IcosahedronGeometry,
  Mesh,
  HemisphereLight,
  SpotLight,
  MeshLambertMaterial,
} from "three";

export default class SingleObject {
  constructor({ context }) {
    this.gsap = context.$gsap;
    this.context = context;
  }

  init({ container }) {
    this.container = container;
    this.createScene();
  }

  createScene() {
    const pixelRatio = window.devicePixelRatio;

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
    });

    this.renderer.setPixelRatio(pixelRatio);

    this.renderer.toneMappingExposure = 0.6;
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.toneMapping = ACESFilmicToneMapping;
    this.renderer.powerPreference = "high-performance";

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = PCFSoftShadowMap;

    this.container.appendChild(this.renderer.domElement);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(
      32,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    this.camera.position.y = 0;
    this.camera.position.z = 10;

    this.camera.lookAt(new Vector3(0, 0, 0));

    this.createLights();
    this.addObjects();
    this.onResize();
    this.render();
  }

  addObjects() {
    // Create cube
    const material = new MeshLambertMaterial({
      color: 'white',
    });
    const obj = new IcosahedronGeometry(1);
    const ico = new Mesh(obj, material);
    ico.name = 'mainObj'
    ico.position.y = 0
    ico.castShadow = true;
    console.log(ico.position)
    this.scene.add(ico);
  }

  createLights() {
    // Create hemisphere light
    const hemisphereLight = new HemisphereLight(0xffffff, 0x080820, 0);
    this.scene.add(hemisphereLight);

    // Create spot light
    const spotLight = new SpotLight(0xffffff, 0, 0, Math.PI);
    spotLight.target.position.set(new Vector3(0, 0, 3));
    spotLight.castShadow = true;
    spotLight.position.set(4, 4, 1);
    spotLight.name = "spot"
    this.scene.add(spotLight);

    const tl = this.gsap.timeline()

    tl.to(hemisphereLight, {
      intensity: 0.1,
      duration: 1,
      ease: "power1.inOut",
    });

    tl.to(spotLight, {
      intensity: 1,
      duration: 2,
      ease: "power1.inOut",
    }, 1);

    document.addEventListener('mousemove', (e) => {
      const ratioX = e.clientX > (window.innerWidth / 2) ? 1 : -1;
      const ratioY = e.clientY > window.innerHeight / 2 ? 1 : -1;
      this.gsap.to(this.getObject({ name: "spot" }).position, {
        x: ratioX * 4,
        y: ratioY * -4,
        duration: 0.2,
        ease: "power1.inOut",
      });
    })
  }

  render() {
    this.renderer.render(this.scene, this.camera);

    const obj = this.getObject({name: 'mainObj'})
    this.gsap.to(obj.rotation, {
      z: obj.rotation.z + 0.02,
      x: obj.rotation.x + 0.02,
    });

    window.requestAnimationFrame(() => {
      this.render();
    });
  }

  onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  getObject({ name }) {
    return this.scene.getObjectByName(name);
  }
}
