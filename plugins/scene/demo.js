import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Group,
  Mesh,
  IcosahedronGeometry,
  Vector3,
  PCFSoftShadowMap,
  ACESFilmicToneMapping,
  Color,
  sRGBEncoding,
  AmbientLight,
  SpotLight,
  MeshLambertMaterial,
} from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";

export default class Three {
  constructor({ context }) {
    this.gsap = context.$gsap;
    this.context = context;
    this.settings = {
      frontLight: {
        color: new Color(0xbe7c7c),
        intensity: 10.6,
        distance: 26.9,
        penumbra: 0,
        x: 10,
        y: 14,
        z: 20,
      },
      backLight: {
        color: new Color(0xc9f0f0),
        intensity: 10,
        distance: 23,
        penumbra: 0,
        x: -19,
        y: -5.3,
        z: 3,
      },
    };
  }

  init({ container }) {
    this.container = container;
    this.createScene();
  }

  createScene() {
    const pixelRatio = window.devicePixelRatio;

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
    });

    this.renderer.setPixelRatio(pixelRatio);

    this.renderer.toneMappingExposure = 0.6;
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.toneMapping = ACESFilmicToneMapping;
    this.renderer.powerPreference = "high-performance";

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = PCFSoftShadowMap;

    this.container.appendChild(this.renderer.domElement);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(
      32,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    this.camera.position.y = 5;
    this.camera.position.z = 10;

    this.camera.lookAt(new Vector3(0, 0, 0));

    const ambientLight = new AmbientLight("blue");
    this.scene.add(ambientLight);

    this.createLights();
    this.addObjects();
    this.onResize();
    this.render();
  }

  addObjects() {
    this.meshesContainer = new Group();
    this.scene.add(this.meshesContainer);

    const objects = [
      {
        size: 1,
        position: {
          x: -1,
          y: -1,
          z: 0,
        },
        speed: -3,
        color: 0x2b6cb0,
      },

      {
        size: 1,
        position: {
          x: 1,
          y: -5,
          z: -10,
        },
        speed: 1,
        color: 0xffff00,
      },

      {
        size: 1,
        position: {
          x: 0,
          y: 0,
          z: -5,
        },
        speed: 4,
        color: 0x9adcff,
      },
    ];

    objects.forEach((el, index) => {
      const material = new MeshLambertMaterial({
        color: el.color,
      });

      const obj = new IcosahedronGeometry(el.size);
      const ico = new Mesh(obj, material);
      ico.name = "mainCube" + index;
      ico.position.set(el.position.x, el.position.y, el.position.z);

      ico.scale.set(0, 0, 0);
      this.meshesContainer.add(ico);

      this.gsap.to(ico.rotation, {
        y: el.speed * 3,
        z: el.speed * 3,
        ease: "power1.inOut",
        scrollTrigger: {
          trigger: "#fullscreen",
          endTrigger: "footer",
          scrub: 1,
          end: "bottom bottom",
        },
      });

      this.gsap.to(
        ico.scale,
        {
          x: 1,
          y: 1,
          z: 1,
          duration: 2,
          ease: "power1.inOut",
          scrollTrigger: {
            trigger: "#fullscreen",
            start: "top top",
            endTrigger: "footer",
            scrub: 1,
            end: "bottom bottom",
          },
        },
        1
      );
    });
  }

  createLights() {
    this.frontLight = new SpotLight(
      this.settings.frontLight.color,
      this.settings.frontLight.intensity
      // this.settings.frontLight.distance
    );

    this.frontLight.castShadow = true;
    this.frontLight.shadow.mapSize.width = 1024; // default
    this.frontLight.shadow.mapSize.height = 1024; // default
    this.frontLight.shadow.camera.near = 0.5; // default
    this.frontLight.shadow.camera.far = 500;
    this.frontLight.shadow.normalBias = 0.2;

    this.scene.add(this.frontLight);

    this.backLight = new SpotLight(
      this.settings.frontLight.color,
      this.settings.frontLight.intensity
      // this.settings.frontLight.distance
    );
    this.backLight.castShadow = true;
    this.backLight.shadow.mapSize.width = 1024; // default
    this.backLight.shadow.mapSize.height = 1024; // default
    this.backLight.shadow.camera.near = 0.5; // default
    this.backLight.shadow.camera.far = 500;
    this.backLight.shadow.normalBias = 0.2;
    this.backLight.penumbra = this.settings.backLight.penumbra;

    this.scene.add(this.backLight);

    this.frontLight.position.set(
      this.settings.frontLight.x,
      this.settings.frontLight.y,
      this.settings.frontLight.z
    );
    this.backLight.position.set(
      this.settings.backLight.x,
      this.settings.backLight.y,
      this.settings.backLight.z
    );
  }

  render() {
    this.renderer.render(this.scene, this.camera);

    window.requestAnimationFrame(() => {
      this.render();
    });
  }

  onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  getObject({ name }) {
    console.log(this.scene);
    return this.scene.getObjectByName(name);
  }
}
