import Three from './scene/demo'
import CreationsScene from "./scene/creations";
import SingleObject from "./scene/lights";
import AudioShader from './scene/audioShader';
import TreeScene from './scene/tree';

export default (context, inject) => {
  const init = () => {
    const scene = new Three({ context: context });
    const creations = new CreationsScene({ context: context });
    const singleObject = new SingleObject({ context: context });
    const audioShader = new AudioShader({ context: context })
    const tree = new TreeScene({ context: context });
    inject("three", scene);
    inject("creations", creations);
    inject("singleObject", singleObject);
    inject("audioShader", audioShader);
    inject("treescene", tree)
  }
  init()
}